const path = require('path');

module.exports = {
  entry: './pihi/static/src/main.js',
  output: {
    path: path.resolve(__dirname, 'pihi/static')
  },
  module: {
    rules: [
      {
        test: /\.riot$/,
        exclude: /node_modules/,
        use: [{
          loader: '@riotjs/webpack-loader',
          options: {
            hot: false, // set it to true if you are using hmr
            // add here all the other @riotjs/compiler options riot.js.org/compiler
            // template: 'pug' for example
          }
        }]
      }
    ]
  }
}