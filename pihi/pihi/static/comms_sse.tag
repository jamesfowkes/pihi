<comms_sse>
    
  <div>
    Test
  </div>

  <script>
    export default {
      onBeforeMount(props, state) {
        this.source = new EventSource(props.sse_url);
        
        this.source.addEventListener('greeting', function(event) {
            var data = JSON.parse(event.data);
            alert("The server says " + data.message);
        }, false);

        this.source.addEventListener('error', function(event) {
            alert("Failed to connect to event stream. Is Redis running?");
        }, false);
      }
    }
  </script>

</comms_sse>
