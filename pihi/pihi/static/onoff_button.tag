<onoff_button>
  <form onsubmit={ changeState }>
      <button class={state.on ? 'button_on' : 'button_off'}>
        {this.text}
      </button>
  </form>

  <script>
    export default {
      onBeforeMount(props, state) {
        // initial state
        this.state = {
          on: false
        }
        this.set_gpio_url = props.set_gpio_url
        this.text = props.text
      },
      changeState(e) {
        e.preventDefault()

        const set_state_data = {
          requested_state: !this.state.on
        }

        const options = {
          method: 'POST',
          body: JSON.stringify(set_state_data),
          headers: {
            'Content-Type': 'application/json'
          }
        }

        let iterator = fetch(this.set_gpio_url, options);

        iterator
          .then(response => {
            return response.json()
           })
          .then(parsed_json => {
            this.state.on = parsed_json.state
            this.update()
          });
        ;
      },
    }
  </script>
</onoff_button>