import logging
import json

from flask import Flask, Blueprint, request, jsonify
from flask_sse import sse

from pihi import app
from pihi import platform

logger = logging.getLogger(__name__)

api = Blueprint('api', __name__)

@api.route("/api/gpio/<gpio>/set", methods=['POST'])
def set_gpio(gpio):
    if request.method == 'POST':
        content = request.get_json()
        requested_state = content["requested_state"]

        this_platform = platform.get_platform()

        if this_platform.gpio_interface:
            gpio = int(gpio)
            this_platform.gpio_interface.setup(gpio, this_platform.gpio_interface.OUT)
            this_platform.gpio_interface.output(gpio, requested_state)
            actual_state = this_platform.gpio_interface.input(gpio)
        else:
            logger.info("No platform interface to set %d to %d", gpio, 1 if requested_state else 0)
            actual_state = requested_state

        return jsonify({"state": actual_state})

@app.route('/hello')
def publish_hello():
    sse.publish({"message": "Hello!"}, type='greeting')
    return "Message sent!"

def init_app(app):
    app.config["REDIS_URL"] = "redis://localhost"
    app.register_blueprint(api)
    app.register_blueprint(sse, url_prefix='/stream')
