import os
import logging

from dataclasses import dataclass

logger = logging.getLogger(__name__)

REVISION_NUM_TO_STR_MAP = {
    frozenset(("0002",)) : "Model B Revision 1.0",
    frozenset(("0003",)) : "Model B Revision 1.0 + ECN0001 (no fuses, D14 removed)",
    frozenset(("0004", "0005", "0006")) : "Model B Revision 2.0 (256MB)",
    frozenset(("0007", "0008", "0009")) : "Model A",
    frozenset(("000d", "000e", "000f")) : "Model B Revision 2.0 (512MB)",
    frozenset(("0010",)) : "Model B+",
    frozenset(("0011",)) : "Compute Module",
    frozenset(("0012",)) : "Model A+",
    frozenset(("a01041", "a21041")) : "Pi 2 Model B",
    frozenset(("900092", "900093")) : "PiZero",
    frozenset(("a02082", "a22082")) : "Pi 3 Model B",
    frozenset(("9000c1",)) : "PiZero W"
}

### Platform Specific IO Modules ###

try:
    import RPi.GPIO as GPIO
    found_rpi_gpio = True
except ImportError:
    logger.warning("Could not import RPi.GPIO")
    found_rpi_gpio = False

####################################
class Platform:

    def __init__(self, name, gpio_nums, gpio_interface):
        self.name = name
        self.gpio_nums = gpio_nums
        self.gpio_interface = gpio_interface

class UnknownPlatform(Platform):
    def __init__(self, gpio_nums=None):
        super().__init__("Unknown", gpio_nums or [], False)

class RaspberryPi(Platform):

    def __init__(self, name):
        super().__init__(name, list(range(2, 28)), GPIO if found_rpi_gpio else None)

        if self.gpio_interface:
            GPIO.setmode(GPIO.BCM)

    def interface(self):
        if self.gpio_interface:
            return GPIO
        else:
            return None

    def __del__(self):
        if self.gpio_interface:
            if GPIO:
                GPIO.cleanup()

def get_cpuinfo():
    try:
        with open("/proc/cpuinfo", "r") as f:
            cpuinfo = f.readlines()
    except Exception as e:
        logger.info("Could not get /proc/cpuinfo: %s", str(e))
        return None
    return cpuinfo

def get_rpi_revision(cpuinfo=None):

    cpuinfo = cpuinfo or get_cpuinfo()
    for l in cpuinfo:
        if l.startswith("Revision"):
            return l.split(":")[1].strip()

    logger.info("Could not find revision in /proc/cpuinfo")
    return None

def get_rpi_revision_string(revision_number=None):

    revision_number = revision_number or get_rpi_revision()

    for revision_nums, revision_str in REVISION_NUM_TO_STR_MAP.items():
        if revision_number in revision_nums:
            return revision_str

    return "Unknown platform"

def _find_platform():

    revision = get_rpi_revision()

    if revision is not None:
        name = get_rpi_revision_string(revision)
        this_platform = RaspberryPi(name)
    else:
        this_platform = UnknownPlatform([1,2,3])

    return this_platform

def get_platform():
    return THIS_PLATFORM

THIS_PLATFORM = _find_platform()

logger.info("Found platform %s", THIS_PLATFORM.name)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    print(get_rpi_revision_string())

