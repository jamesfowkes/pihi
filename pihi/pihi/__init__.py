import os
import logging
import logging.handlers

from pathlib import Path

from flask import Flask

APP_PATH = Path(__file__).parent

app = Flask(__name__)

from pihi.views import basic

# Initialise blueprints after the views have been imported to correctly register endpoints
from pihi.blueprints import api

import pihi.platform

if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

api.init_app(app)
