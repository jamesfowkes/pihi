import logging

from flask import render_template

from pihi import app
from pihi import platform

@app.route('/')
def index():

    this_platform = platform.get_platform()

    return render_template("index.html", platform=this_platform)
